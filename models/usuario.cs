﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace PruebaCasa.models
{
    public class Usuario
    {
        public int Dni { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Visible { get; set;}
        public DateTime Fechadecreacion { get; set; }
        public DateTime Fechademodificacion { get; set; }
        public DateTime Fechadebaja { get; set; }

    }
}
