﻿using PruebaCasa;
using PruebaCasa.models;

internal  class Program
{
    private static void Main(string[] args)
    {
        var db = new Bd();     //abrimos en enlase con la base

        var persona = new Usuario();         //instanciamos persona
        Console.WriteLine("ingrese dni: ");
        persona.Dni = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("ingrese nombre: ");
        persona.Nombre = Console.ReadLine();
        Console.WriteLine("ingrese apellido: ");
        persona.Apellido = Console.ReadLine();

        //db.AgregarPersona(persona);         // agregar persona a la tabla

        Console.ReadLine();                   //

        //var lista = db.LeeerTabla();          // mostrar la tabla con los usuarios visibles
        //foreach (var i in lista)
        //{
        //    Console.WriteLine($"{i.Dni} {i.Nombre} {i.Apellido} fue dada de alta el {i.Fechadecreacion}");
        //}

        //db.ModificarPersona(persona);

        //db.EliminarData(persona); //cambiar visibilidad

        db.DarDeAlta(persona);
    }
}