﻿using Dapper;
using Npgsql;
using PruebaCasa.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaCasa
{
    public class Bd
    {
        private readonly string _conexionstring = "server=localhost;port=5432;dataBase= prueba1;user id = postgres;password=dev";
        private object data;

        public NpgsqlConnection conectar()
        {
            return new NpgsqlConnection(_conexionstring);
        }
        public void AgregarPersona(Usuario data)     // agregamos un usuario a la tabla
        {
            var query = "insert into personas (dni,nombre,apellido,fechadecreacion) values (@dni,@nombre,@apellido,@fechadecreacion)";
            var db = new Bd();
            var con = db.conectar();
            con.Open();
            con.Query(query, new
            {
                dni = data.Dni,
                nombre = data.Nombre,
                apellido = data.Apellido,
                fechadecreacion = DateTime.Today
            });
            con.Close();
        }
        public List<Usuario> LeeerTabla()                //mostramos la tabla con los usuarios visibles
        {
            var query = "select * from personas p where p.visible = 1";
            var db = new Bd();
            var con = db.conectar();
            con.Open();
            var resultado = con.Query<Usuario>(query);
            con.Close();
            return resultado.ToList();
        }

        public void ModificarPersona(Usuario data)          //modificar los campos de una persona
        {
            var query = "update Personas set nombre = @nombre, apellido=@apellido where dni=@dni";
            var db = new Bd();
            var con = db.conectar();
            con.Open();
            con.Query(query, new
            {
                dni = data.Dni,
                nombre = data.Nombre,
                apellido = data.Apellido,
                fechademodificacion = DateTime.Today
            });
            con.Close();
        }

        public void EliminarData(Usuario data)
        {
            var query = "update personas set visible = 0,fechadebaja=@fechadebaja where dni=@dni";
            var bd = new Bd();
            var con = bd.conectar();
            con.Open();
            con.Query(query, new
            {
                dni = data.Dni,
                fechadebaja = DateTime.Today,
            });
            con.Close();

        }

        public void DarDeAlta(Usuario data)
        {
            var query = "update personas set visible = 1,fechademodificacion = @fechademodificacion where dni=@dni ";
            var Db = new Bd();
            var con = Db.conectar();
            con.Open();
            con.Query(query, new
            { 
                dni = data.Dni,
                Fechademodificacion = DateTime.Today
            });
            con.Close();
        }
    }
}
